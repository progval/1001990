class VariableDoesNotExist(Exception):
    pass

class MyClass:
    # ...
    def makeDb(self)
        cursor = self._connection.cursor()
        cursor.execute("""CREATE TABLE variables (
                          domainType TEXT,
                          domainName TEXT,
                          variableName TEXT,
                          value TEXT,
                          sticky BOOLEAN
                          )""")
        self._connection.commit()

    def _getVariable(self, domainName, domainType, variableName):
        print 'get: %r' % ((domainName, domainType, variableName),) # DEBUG
        cursor = self._connection.cursor()
        cursor.execute("""SELECT * FROM variables WHERE
                          domainType=? AND domainName=? AND variableName=?""",
                          (domainType, domainName, variableName))
        row = cursor.fetchone()
        print repr(row) # DEBUG
        if row is None:
            raise VariableDoesNotExist()
        else:
            return row[0]

    def set(self, name, value):
        domainType, domainName = self._getDomain()
        try:
            self._getVariable(domainType, domainName, name)
            cursor.execute("""DELETE FROM variables WHERE
                              domainType=? AND domainName=? AND
                              variableName=?""",
                          (domainType, domainName, name))
        except VariableDoesNotExist:
            pass
        print 'set: %r' % ((domainType, domainName, name, value),) # DEBUG
        cursor.execute("""INSERT INTO variables VALUES (?,?,?,?,?)""",
                          (domainType, domainName, name, value, False))
        cursor.execute("""SELECT * FROM variables""")
        print repr(cursor.fetchall()) # DEBUG
        self._connection.commit()
    def get(self, name):
        domainType, domainName = self._getDomain()
        try:
            print self._getVariable(domainType, domainName, name)) # DEBUG
        except VariableDoesNotExist:
            print 'Variable does not exist.'


    def testSetGet(self):
        self.get('foo')
        self.set('foo', 'bar')
        self.get('foo')
        self.set('foo' 'baz')
        self.get('foo')

myObject = MyClass()
myObject.testSetGet()